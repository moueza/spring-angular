package com.bushansirgur.spring.config;

import static org.hibernate.cfg.Environment.*;
import java.util.Properties;

import org.hibernate.cfg.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.hibernate.cfg.Environment.*;//DRIVER
import java.util.Properties;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = { "com.bushansirgur.spring.controller" })
public class WebConfig extends WebMvcConfigurerAdapter {

}
