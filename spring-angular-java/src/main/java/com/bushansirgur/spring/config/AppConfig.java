package com.bushansirgur.spring.config;

import static org.hibernate.cfg.Environment.*;
import java.util.Properties;

//import org.hibernate.cfg.Environment;
import org.springframework.core.env.Environment;/**https://www.mkyong.com/spring/spring-propertysources-example/*/
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.hibernate.cfg.Environment.*;//DRIVER
import java.util.Properties;

@Configuration
@PropertySource("classpath:db.properties")
@EnableTransactionManagement
@ComponentScans(value = { @ComponentScan("com.bushansirgur.spring.dao"),
		@ComponentScan("com.bushansirgur.spring.service") })
public class AppConfig {
	@Autowired
	private Environment env;

	@Bean
	LocalSessionFactoryBean getSessionFactory() {

		LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();
		Properties props = new Properties();

		// Setting JDBC properties
		props.put(DRIVER, env.getProperty("mysql.driver"));
		// props.put(DRIVER, env.getProperties().getProperty("mysql.driver"));

		props.put(URL, env.getProperty("mysql.url"));
		// props.put(URL, env.getProperties().getProperty("mysql.url"));

		props.put(USER, env.getProperty("mysql.user"));
		// props.put(USER, env.getProperties().getProperty("mysql.user"));

		props.put(PASS, env.getProperty("mysql.password"));
		// props.put(PASS, env.getProperties().getProperty("mysql.password"));

		// setting Hibernate properties
		props.put(C3P0_MIN_SIZE, env.getProperty("hibernate.c3p0.min_size"));
		// props.put(C3P0_MIN_SIZE,
		// env.getProperties().getProperty("hibernate.c3p0.min_size"));

		props.put(C3P0_MAX_SIZE, env.getProperty("hibernate.c3p0.max_size"));
		// props.put(C3P0_MAX_SIZE,
		// env.getProperties().getProperty("hibernate.c3p0.max_size"));

		props.put(C3P0_ACQUIRE_INCREMENT, env.getProperty("hibernate.c3p0.acquire_increment"));
		// props.put(C3P0_ACQUIRE_INCREMENT,
		// env.getProperties().getProperty("hibernate.c3p0.acquire_increment"));

		props.put(C3P0_TIMEOUT, env.getProperty("hibernate.c3p0.timeout"));
		// props.put(C3P0_TIMEOUT,
		// env.getProperties().getProperty("hibernate.c3p0.timeout"));

		props.put(C3P0_MAX_STATEMENTS, env.getProperty("hibernate.c3p0.max_statements"));
		// props.put(C3P0_MAX_STATEMENTS,
		// env.getProperties().getProperty("hibernate.c3p0.max_statements"));

		factoryBean.setHibernateProperties(props);
		factoryBean.setPackagesToScan("com.bushansirgur.spring.model");

		return factoryBean;

	}

	public HibernateTransactionManager getTransactionManager() {
		HibernateTransactionManager transactionManager = new HibernateTransactionManager();
		transactionManager.setSessionFactory(getSessionFactory().getObject());
		return transactionManager;
	}
}
